Rails.application.routes.draw do

  devise_for :managers, path: 'auth', controllers: { 
    confirmations: 'auth/confirmations',
    passwords: 'auth/passwords',
    registrations: 'auth/registrations',
    sessions: 'auth/sessions',
    unlocks: 'auth/unlocks',
  }

  root "home#index"
end
